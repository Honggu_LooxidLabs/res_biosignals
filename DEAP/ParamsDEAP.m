function params = ParamsDEAP()
%% PRE
params.sampling_rate = 128;
params.bands         = [1 4; 4 8; 8 12; 12 30; 30 45]; % delta, theta, alpha, beta, and gamma
params.filt_type     = 'iir';
params.butter_order  = 4;
params.num_ch        = 32;

%% FEAT 
params.feat_type = 'filt';
params.hamm_on  = 1;
params.window   = 5;   % sec
params.overlap  = 2.5;    % sec
params.nsi_seg  = 25;   % samples
params.fd_k     = 12;   % samples
params.hoc_j    = 10;   % maximum order of the estimated HOC (used 10 in the ref. paper)
params.num_fft  = 2^12; % 2^10
params.stft_win = params.sampling_rate/1;
params.hhs_w    = 1;    % number of segments 
params.dwt_l    = 4;    % db4

end