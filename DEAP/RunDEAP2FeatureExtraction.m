clear
close all

%% FIND SUBJECTS MAT DATA
addpath(genpath('../Function'));
data_path       = '/home/honggu/Dataset/DEAP (Mat-Preprocessing)/';
subjects_list   = dir(data_path);
params          = ParamsDEAP();

win_cand = [10];
%% DO SOMETHING
for win = 1:length(win_cand)
    params.window = win_cand(win);
    params.overlap = 0;
    
    %% SAVE PATH
    save_base_path  = '/home/honggu/Dataset/DEAP (Mat-Features)/';
    save_folder     = sprintf('win_%d_over_%d', params.window, params.overlap) ;
    mkdir([save_base_path, save_folder]);
    
    for s = 3:length(subjects_list)
        subject_file = subjects_list(s).name;
        load([data_path, subject_file])
        
        %% FEATURE EXTRACTION
        feat = [];
        feat_cnt = 1;
        [feat, feat_cnt, feat_ck] = FeatStatistics( eeg, feat, feat_cnt, params );
        [feat, feat_cnt] = FeatHjorth( eeg, feat, feat_cnt, params );
        [feat, feat_cnt] = FeatNSI( eeg, feat, feat_cnt, params );
        [feat, feat_cnt] = FeatFD( eeg, feat, feat_cnt, params );
        [feat, feat_cnt] = FeatHOC( eeg, feat, feat_cnt, params );
        [feat, feat_cnt] = FeatBandPower( eeg, feat, feat_cnt, params, 'fft' );
        [feat, feat_cnt] = FeatPSD( eeg, feat, feat_cnt, params );
        [feat, feat_cnt] = FeatHHS( eeg, feat, feat_cnt, params );
        [feat, feat_cnt] = FeatDWT( eeg, feat, feat_cnt, params );
        
        save([save_base_path, save_folder, '/', eeg.subject, '.mat'], 'feat', 'feat_ck');
        fprintf('$$$$ %s: Feature Extraction Complete. $$$$ \n', subject_file(1:3));
    end
    
end
