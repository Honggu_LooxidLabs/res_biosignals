clear
close all

%% FIND SUBJECTS MAT DATA
addpath(genpath('Function'));
addpath(genpath('/home/honggu/Projects/3rdParty/ICA'));
addpath(genpath('/home/honggu/Projects/3rdParty/BrainWavelet'));
data_path       = '/home/honggu/Projects/Dataset/EVA_ADWave 2017 (Mat)/';
subjects_list   = dir(data_path);
params          = Params();

%% SAVE PATH
save_base_path  = '/home/honggu/Projects/Dataset/EVA_ADWave 2017 (Mat-Preprocessing)/';
save_folder     = sprintf('%s_o%d_s%d_%s_%s', params.filt_type, params.butter_order, params.stop_fq, params.ar_j.wavelet, params.ar_j.nscale);
mkdir([save_base_path, save_folder]);

%% DO SOMETHING
for s = 3:length(subjects_list)
    subject_file = subjects_list(s).name;
    load([data_path, subject_file]);

    if exist([save_base_path, save_folder, '/' , subject_file], 'file') ~= 2
        if sum(data.session) == 4
            eeg = [];
            eeg.subject = data.subject;
            eeg.survey  = zeros(40, 7);
            for session = 1:4
                if data.session(session) == 1
                    %% CALIBRATION DATA PREPROCESSING
                    eeg.calib.raw   = data.calibration(session).open.eeg;
                    %                 eeg.calib.de_t  = PreDetrend(eeg.calib.raw);
                    eeg.calib.filt  = PreFiltering(eeg.calib.raw, params, params.filt_type);
                    eeg.calib.ar_j  = ArtifactRemoval(eeg.calib.filt, params, 'jump');
                    eeg.calib.ar_b  = ArtifactRemoval(eeg.calib.ar_j, params, 'blink');
                    
                    for vid = 1:10
                        if ~isempty(data.contents((session-1)*10+vid).video)
                            %% BASELINE AND VIDEO DATA PREPROCESSING
                            vid_num     = (session-1)*10 + vid;
                            
                            eeg.base(vid_num).raw   = data.contents(vid_num).baseline.eeg;
                            %                         eeg.base(vid_num).de_t  = PreDetrend(eeg.base(vid_num).raw);
                            eeg.base(vid_num).filt  = PreFiltering(eeg.base(vid_num).raw, params, params.filt_type);
                            eeg.base(vid_num).ar_j  = ArtifactRemoval(eeg.base(vid_num).filt, params, 'jump');
                            eeg.base(vid_num).ar_b  = ArtifactRemoval(eeg.base(vid_num).ar_j, params, 'blink');
                            fprintf('%s-%.2d: Baseline Preprocessing Done.\n', eeg.subject, vid_num)
                            
                            eeg.vid(vid_num).raw   = data.contents(vid_num).video.eeg;
                            %                         eeg.vid(vid_num).de_t  = PreDetrend(eeg.vid(vid_num).raw);
                            eeg.vid(vid_num).filt  = PreFiltering(eeg.vid(vid_num).raw, params, params.filt_type);
                            eeg.vid(vid_num).ar_j  = ArtifactRemoval(eeg.vid(vid_num).filt, params, 'jump');
                            eeg.vid(vid_num).ar_b  = ArtifactRemoval(eeg.vid(vid_num).ar_j, params, 'blink');
                            fprintf('%s-%.2d: Video Preprocessing Done.\n', eeg.subject, vid_num)
                            
                            %% SURVEY
                            eeg.survey(vid_num, :) = data.contents(vid_num).survey.rating(:,3)';
                        end
                    end
                end
            end
            save([save_base_path, save_folder, '/' , eeg.subject, '.mat'], 'eeg');
            fprintf('%s Done\n', eeg.subject)
        end
    end
end
