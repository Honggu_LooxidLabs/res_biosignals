function  [feat, feat_cnt] = FeatNSI( data, feat, feat_cnt, params )
%% NSI FEATURE LIST
feat(feat_cnt).name = 'nsi';
feat(feat_cnt).idx  = [];
feat(feat_cnt).data = [];
feat(feat_cnt).label = []; 

%% EXTRACTION
for vid = 1:size(data.vid, 2)
    data_t = data.vid(vid).(params.feat_type)'; % ch x tim
    
    if ~isempty(data_t)
        idx     = 1:size(data_t, 2);
        if length(idx) < ceil(params.overlap * params.sampling_rate)
            idx_m = idx';
        else
            idx_m   = buffer(idx, ceil(params.window * params.sampling_rate), ceil(params.overlap * params.sampling_rate), 'nodelay');
        end
        idx_m_seg = buffer(idx_m(:, 1), ceil(params.window * params.nsi_seg), 0, 'nodelay');
        
        nsi_v = [];
        if sum(idx_m(:, end) == 0) && size(idx_m, 2) ~= 1
            num_seg = size(idx_m, 2) - 1;
        else 
            num_seg = size(idx_m, 2);
        end
        for i = 1:num_seg
            idx_m_i = idx_m(:, i);
            data_t_i = data_t(:, idx_m_i(idx_m_i ~= 0));
            
            if params.hamm_on
                data_t_i = data_t_i .* repmat(hamming(ceil(size(data_t_i, 2)))', params.num_ch, 1);
            end
            
            sigma   = std(data_t_i, [], 2);
            norm    = data_t_i ./ sigma;
            norm_seg_mean = [];
%             for j = 1:size(idx_m_seg, 2)
            for j = 1:sum(sum(idx_m_seg) ~= 0)
                idx_m_seg_j = idx_m_seg(:, j);
                idx_m_seg_j(idx_m_seg_j == 0) = [];
                if size(norm, 2) >= max(idx_m_seg_j)
                    norm_seg = norm(:, idx_m_seg_j);
                end
                norm_seg_mean = [norm_seg_mean, mean(norm_seg, 2)];
            end
            nsi_i = std(norm_seg_mean, [], 2);
            nsi_v = [nsi_v, nsi_i];
        end
        
        feat(feat_cnt).data = [feat(feat_cnt).data; nsi_v'];
        feat(feat_cnt).idx  = [feat(feat_cnt).idx; repmat(vid, size(nsi_v, 2), 1)]; 
        feat(feat_cnt).label = [feat(feat_cnt).label; repmat(data.survey(vid, :), size(nsi_v, 2), 1)]; 
    end
end
feat_cnt = feat_cnt + 1;
fprintf('Done: Feature Extraction (NSI)\n');

