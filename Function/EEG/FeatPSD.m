function [feat, feat_cnt] = FeatPSD( data, feat, feat_cnt, params )
%% PSD FEATURE LIST
for i = 1:size(params.bands, 1)
    feat(feat_cnt + i - 1).name = ['psd_avg_b_', num2str(i)];
    feat(feat_cnt + i - 1).idx  = [];
    feat(feat_cnt + i - 1).data = [];
    feat(feat_cnt + i - 1).label = [];
    feat(feat_cnt + i - 1 + size(params.bands, 1)).name = ['psd_min_b_', num2str(i)];
    feat(feat_cnt + i - 1 + size(params.bands, 1)).idx  = [];
    feat(feat_cnt + i - 1 + size(params.bands, 1)).data = [];
    feat(feat_cnt + i - 1 + size(params.bands, 1)).label = [];
    feat(feat_cnt + i - 1 + size(params.bands, 1)*2).name = ['psd_max_b_', num2str(i)];
    feat(feat_cnt + i - 1 + size(params.bands, 1)*2).idx  = [];
    feat(feat_cnt + i - 1 + size(params.bands, 1)*2).data = [];
    feat(feat_cnt + i - 1 + size(params.bands, 1)*2).label = [];
    feat(feat_cnt + i - 1 + size(params.bands, 1)*3).name = ['psd_var_b_', num2str(i)];
    feat(feat_cnt + i - 1 + size(params.bands, 1)*3).idx  = [];
    feat(feat_cnt + i - 1 + size(params.bands, 1)*3).data = [];
    feat(feat_cnt + i - 1 + size(params.bands, 1)*3).label = [];
end
feat(end + 1).name  = 'psd_ab_ratio';
feat(end).idx   = [];
feat(end).data  = [];
feat(end).label = [];

%% EXTRACTION
for vid = 1:size(data.vid, 2)
    data_t = data.vid(vid).(params.feat_type)'; % ch x time
    
    if ~isempty(data_t)
        idx     = 1:size(data_t, 2);
        if length(idx) < ceil(params.overlap * params.sampling_rate)
            idx_m = idx';
        else
            idx_m = buffer(idx, ceil(params.window * params.sampling_rate), ceil(params.overlap * params.sampling_rate), 'nodelay');
        end
        
        psd_avg_v = [];
        psd_min_v = [];
        psd_max_v = [];
        psd_var_v = [];
        if sum(idx_m(:, end) == 0) && size(idx_m, 2) ~= 1
            num_seg = size(idx_m, 2) - 1;
        else 
            num_seg = size(idx_m, 2);
        end
        for i = 1:num_seg
            idx_m_i = idx_m(:, i);
            data_i  = data_t(:, idx_m_i(idx_m_i ~= 0));
            
            switch params.filt_type
                case 'iir'
                    filt_order = params.butter_order;
                    bands_psd.avg = [];
                    bands_psd.min = [];
                    bands_psd.max = [];
                    bands_psd.var = [];
                    for b = 1:length(params.bands)
                        [B, A] = butter(filt_order, params.bands(b, :)/(params.sampling_rate/2), 'bandpass');
                        data_bandpass   = filtfilt(B, A, data_i');
                        pxx_welch       = pwelch(data_bandpass);
                        bands_psd.avg   = [bands_psd.avg, mean(pxx_welch)'];
                        bands_psd.min   = [bands_psd.min, min(pxx_welch)'];
                        bands_psd.max   = [bands_psd.max, max(pxx_welch)'];
                        bands_psd.var   = [bands_psd.var, var(pxx_welch)'];
                    end
                    
                case 'fir'
                    filt_order = 3*fix(params.sampling_rate / max(params.bandpass_fq) );
                    if filt_order > floor((size(data_i', 1) - 1) / 3)
                        filt_order = floor(size(data_i', 1)/3) - 1;
                    end
                    bands_psd.avg = [];
                    bands_psd.min = [];
                    bands_psd.max = [];
                    bands_psd.var = [];
                    for b = 1:length(params.bands)
                        B = fir1(filt_order, [params.bands(b,1) params.bands(b,2)]/params.sampling_rate);
                        A = 1;
                        data_bandpass   = filtfilt(B, A, data_i');
                        pxx_welch       = pwelch(data_bandpass);
                        bands_psd.avg   = [bands_psd.avg, mean(pxx_welch)'];
                        bands_psd.min   = [bands_psd.min, min(pxx_welch)'];
                        bands_psd.max   = [bands_psd.max, max(pxx_welch)'];
                        bands_psd.var   = [bands_psd.var, var(pxx_welch)'];
                    end
            end
            psd_avg_v = [psd_avg_v, bands_psd.avg];
            psd_min_v = [psd_min_v, bands_psd.min];
            psd_max_v = [psd_max_v, bands_psd.max];
            psd_var_v = [psd_var_v, bands_psd.var];
        end
        
        for i = 1:size(params.bands, 1)
            feat(feat_cnt + i - 1).idx  = [feat(feat_cnt + i - 1).idx; repmat(vid, size(psd_avg_v, 2)/size(params.bands, 1), 1)];
            feat(feat_cnt + i - 1).data = [feat(feat_cnt + i - 1).data; psd_avg_v(:, i:size(params.bands, 1):end)'];
            feat(feat_cnt + i - 1).label = [feat(feat_cnt + i - 1).label; repmat(data.survey(vid, :), size(psd_avg_v, 2)/size(params.bands, 1), 1)];
            feat(feat_cnt + i - 1 + size(params.bands, 1)).idx  = [feat(feat_cnt + i - 1 + size(params.bands, 1)).idx; repmat(vid, size(psd_min_v, 2)/size(params.bands, 1), 1)];
            feat(feat_cnt + i - 1 + size(params.bands, 1)).data = [feat(feat_cnt + i - 1 + size(params.bands, 1)).data; psd_min_v(:, i:size(params.bands, 1):end)'];
            feat(feat_cnt + i - 1 + size(params.bands, 1)).label = [feat(feat_cnt + i - 1 + size(params.bands, 1)).label; repmat(data.survey(vid, :), size(psd_min_v, 2)/size(params.bands, 1), 1)];
            feat(feat_cnt + i - 1 + size(params.bands, 1)*2).idx  = [feat(feat_cnt + i - 1 + size(params.bands, 1)*2).idx; repmat(vid, size(psd_max_v, 2)/size(params.bands, 1), 1)];
            feat(feat_cnt + i - 1 + size(params.bands, 1)*2).data = [feat(feat_cnt + i - 1 + size(params.bands, 1)*2).data; psd_max_v(:, i:size(params.bands, 1):end)'];
            feat(feat_cnt + i - 1 + size(params.bands, 1)*2).label = [feat(feat_cnt + i - 1 + size(params.bands, 1)*2).label; repmat(data.survey(vid, :), size(psd_max_v, 2)/size(params.bands, 1), 1)];
            feat(feat_cnt + i - 1 + size(params.bands, 1)*3).idx  = [feat(feat_cnt + i - 1 + size(params.bands, 1)*3).idx; repmat(vid, size(psd_var_v, 2)/size(params.bands, 1), 1)];
            feat(feat_cnt + i - 1 + size(params.bands, 1)*3).data = [feat(feat_cnt + i - 1 + size(params.bands, 1)*3).data; psd_var_v(:, i:size(params.bands, 1):end)'];
            feat(feat_cnt + i - 1 + size(params.bands, 1)*3).label = [feat(feat_cnt + i - 1 + size(params.bands, 1)*3).label; repmat(data.survey(vid, :), size(psd_var_v, 2)/size(params.bands, 1), 1)];
        end
    end
end
feat(end).data  = feat(end-17).data ./ feat(end-18).data; % beta/alpha ratio
feat(end).idx   = feat(end-17).idx;
feat(end).label = feat(end-17).label;
feat_cnt = feat_cnt + (4*5 + 1);
fprintf('Done: Feature Extraction (PSD)\n');

