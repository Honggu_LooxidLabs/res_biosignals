function  [feat, feat_cnt ] = FeatHjorth( data, feat, feat_cnt, params )

%% HJORTH FEATURE LIST
feat(feat_cnt + 0).name = 'hjorth_mobility';
feat(feat_cnt + 1).name = 'hjorth_complexity';
for i = 1:2
    feat(feat_cnt + i - 1).idx  = [];
    feat(feat_cnt + i - 1).data = [];
    feat(feat_cnt + i - 1).label = [];
end

%% EXTRACTION
for vid = 1:size(data.vid, 2)
    data_t = data.vid(vid).(params.feat_type)'; % ch x time
    
    if ~isempty(data_t)
        idx     = 1:size(data_t, 2);
        if length(idx) < ceil(params.overlap * params.sampling_rate)
            idx_m = idx';
        else
            idx_m = buffer(idx, ceil(params.window * params.sampling_rate), ceil(params.overlap * params.sampling_rate), 'nodelay');
        end
        
        mobility_v      = [];
        complexity_v    = [];
        if sum(idx_m(:, end) == 0) && size(idx_m, 2) ~= 1
            num_seg = size(idx_m, 2) - 1;
        else 
            num_seg = size(idx_m, 2);
        end
        for i = 1:num_seg
            idx_m_i = idx_m(:, i);
            data_t_i = data_t(:, idx_m_i(idx_m_i ~= 0));
            
            if params.hamm_on
                data_t_i = data_t_i .* repmat(hamming(ceil(size(data_t_i, 2)))', params.num_ch, 1);
            end
            sigma = std(data_t_i, [], 2);
            derivation1 = diff(data_t_i, 1, 2) / (1/params.sampling_rate);
            derivation2 = diff(derivation1, 1, 2) / (1/params.sampling_rate);
            
%             activity    = sigma.^2; % h1: squared standard deviation of the eeg signal.
            mobility_i    = sqrt(var(derivation1, [], 2) ./ var(data_t_i, [], 2));
            complexity_i  = sqrt(var(derivation2, [], 2) ./ var(derivation1, [], 2)) ./ mobility_i;
            
            mobility_v   = [mobility_v, mobility_i];
            complexity_v = [complexity_v, complexity_i];
        end
        
        feat(feat_cnt + 0).idx  = [feat(feat_cnt + 0).idx; repmat(vid, size(mobility_v, 2), 1)];
        feat(feat_cnt + 0).data = [feat(feat_cnt + 0).data; mobility_v'];
        feat(feat_cnt + 0).label = [feat(feat_cnt + 0).label; repmat(data.survey(vid, :), size(mobility_v, 2), 1)];
        
        feat(feat_cnt + 1).idx  = [feat(feat_cnt + 1).idx; repmat(vid, size(complexity_v, 2), 1)];
        feat(feat_cnt + 1).data = [feat(feat_cnt + 1).data; complexity_v'];
        feat(feat_cnt + 1).label = [feat(feat_cnt + 1).label; repmat(data.survey(vid, :), size(complexity_v, 2), 1)];
    end
end
feat_cnt = feat_cnt + 2;
fprintf('Done: Feature Extraction (Hjorth)\n');

%% SINGLE VIDEO
% function  [feat] = FeatHjorth( data, params )
% data_t  = data'; % ch x time
% idx     = 1:size(data_t, 2);
% idx_m   = buffer(idx, ceil(params.window * params.sampling_rate), ceil(params.overlap * params.sampling_rate), 'nodelay');
% 
% % feat.activity = [];
% feat.mobility = [];
% feat.complexity = [];
% for i = 1:size(idx_m, 2)
%     idx_m_i = idx_m(:, i);
%     data_t_i = data_t(:, idx_m_i(idx_m_i ~= 0));
%     
%     if params.hamm_on
%         data_t_i = data_t_i .* repmat(hamming(ceil(size(data_t_i, 2)))', params.num_ch, 1);
%     end
%     sigma = std(data_t_i, [], 2);
%     derivation1 = diff(data_t_i, 1, 2) / (1/params.sampling_rate);
%     derivation2 = diff(derivation1, 1, 2) / (1/params.sampling_rate);
%     
%     activity    = sigma.^2; % h1: squared standard deviation of the eeg signal.
%     mobility    = sqrt(var(derivation1, [], 2) ./ var(data_t_i, [], 2));
%     complexity  = sqrt(var(derivation2, [], 2) ./ var(derivation1, [], 2)) ./ mobility;
%     
% %     feat.activity   = [feat.activity, activity];
%     feat.mobility   = [feat.mobility, mobility];
%     feat.complexity = [feat.complexity, complexity];
% end