function [ feat, feat_cnt ] = FeatDWT( data, feat, feat_cnt, params )
%% DWT FEATURE LIST
for i = 1:params.dwt_l
    feat(feat_cnt + i - 1).name = ['dwt_rpe_b', num2str(i)];
    feat(feat_cnt + i - 1).idx  = [];
    feat(feat_cnt + i - 1).data = [];
    feat(feat_cnt + i - 1).label = [];
    feat(feat_cnt + i - 1 + params.dwt_l).name = ['dwt_lrpe_b', num2str(i)];
    feat(feat_cnt + i - 1 + params.dwt_l).idx  = [];
    feat(feat_cnt + i - 1 + params.dwt_l).data = [];
    feat(feat_cnt + i - 1 + params.dwt_l).label = [];
    feat(feat_cnt + i - 1 + params.dwt_l*2).name = ['dwt_alrpe_b', num2str(i)];
    feat(feat_cnt + i - 1 + params.dwt_l*2).idx  = [];
    feat(feat_cnt + i - 1 + params.dwt_l*2).data = [];
    feat(feat_cnt + i - 1 + params.dwt_l*2).label = [];
end

%% EXTRACTION
for vid = 1:size(data.vid, 2)
    data_t = data.vid(vid).(params.feat_type)'; % ch x time
    
    if ~isempty(data_t)
        idx         = 1:size(data_t, 2);
        if length(idx) < ceil(params.overlap * params.sampling_rate)
            idx_m = idx';
        else
            idx_m   = buffer(idx, ceil(params.window * params.sampling_rate), ceil(params.overlap * params.sampling_rate), 'nodelay');
        end
        
        dwt_all.rpe     = [];
        dwt_all.lrpe    = [];
        dwt_all.alrpe   = [];
        if sum(idx_m(:, end) == 0) && size(idx_m, 2) ~= 1
            num_seg = size(idx_m, 2) - 1;
        else 
            num_seg = size(idx_m, 2);
        end
        for i = 1:num_seg
            idx_m_i = idx_m(:, i);
            data_i  = data_t(:, idx_m_i(idx_m_i ~= 0));
            
            dwt_rpe     = [];
            dwt_lrpe    = [];
            dwt_alrpe   = [];
            for ch = 1:size(data_i, 1)
                data_ch = data_i(ch, :);
                [pe, ~] = dwt(data_ch, params);
                rpe     = pe./sum(pe);
                lrpe    = log(rpe);
                alrpe   = abs(lrpe);
                dwt_rpe     = [dwt_rpe; rpe];
                dwt_lrpe    = [dwt_lrpe; lrpe];
                dwt_alrpe   = [dwt_alrpe; alrpe];
            end
            dwt_all.rpe     = [dwt_all.rpe, dwt_rpe];
            dwt_all.lrpe    = [dwt_all.lrpe, dwt_lrpe];
            dwt_all.alrpe   = [dwt_all.alrpe, dwt_alrpe];
        end
        
        for i = 1:params.dwt_l
            feat(feat_cnt + i - 1).idx  = [feat(feat_cnt + i - 1).idx; repmat(vid, size(dwt_all.rpe, 2)/params.dwt_l, 1)];
            feat(feat_cnt + i - 1).data = [feat(feat_cnt + i - 1).data; dwt_all.rpe(:, i:params.dwt_l:end)'];
            feat(feat_cnt + i - 1).label = [feat(feat_cnt + i - 1).label; repmat(data.survey(vid, :), size(dwt_all.rpe, 2)/params.dwt_l, 1)];
            feat(feat_cnt + i - 1 + params.dwt_l).idx  = [feat(feat_cnt + i - 1 + params.dwt_l).idx; repmat(vid, size(dwt_all.lrpe, 2)/params.dwt_l, 1)];
            feat(feat_cnt + i - 1 + params.dwt_l).data = [feat(feat_cnt + i - 1 + params.dwt_l).data; dwt_all.lrpe(:, i:params.dwt_l:end)'];
            feat(feat_cnt + i - 1 + params.dwt_l).label = [feat(feat_cnt + i - 1 + params.dwt_l).label; repmat(data.survey(vid, :), size(dwt_all.lrpe, 2)/params.dwt_l, 1)];
            feat(feat_cnt + i - 1 + params.dwt_l*2).idx  = [feat(feat_cnt + i - 1 + params.dwt_l*2).idx; repmat(vid, size(dwt_all.alrpe, 2)/params.dwt_l, 1)];
            feat(feat_cnt + i - 1 + params.dwt_l*2).data = [feat(feat_cnt + i - 1 + params.dwt_l*2).data; dwt_all.alrpe(:, i:params.dwt_l:end)'];
            feat(feat_cnt + i - 1 + params.dwt_l*2).label = [feat(feat_cnt + i - 1 + params.dwt_l*2).label; repmat(data.survey(vid, :), size(dwt_all.alrpe, 2)/params.dwt_l, 1)];
        end
    end
end
feat_cnt = feat_cnt + 3 * params.dwt_l;
fprintf('Done: Feature Extraction (DWT)\n');
end

function [pe, rms] = dwt(signal, params)
% L: decompose levels
rms = zeros(1, params.dwt_l);
for k = 1:params.dwt_l
    [signal coeff{k}] = afb(signal, params.bands);
    pe(k) = sum(coeff{k}.^2);
%     sd(k) = std(coeff{k});
    len(k) = length(coeff{k});
    rms(k) = rms(k) + sqrt(sum(pe(1:k))/sum(len(1:k)));
end
% coeff{params.dwt_l+1} = signal;
end

% Analysis filter bank
function [lo, hi] = afb(signal, bands)
N = length(signal);
L = floor(length(bands)/2);
signal = cshift(signal, -L);

% lowpass filter
lo = upfirdn(signal, bands(:,1), 1, 2);
lo(1:L) = lo(floor(N/2+(1:L))) + lo(1:L);
% lo = lo(1:N/2);
lo = lo(1:floor(N/2));

% highpass filter
hi = upfirdn(signal, bands(:,2), 1, 2);
hi(1:L) = hi(floor(N/2+(1:L))) + hi(1:L);
% hi = hi(1:N/2);
hi = hi(1:floor(N/2));
end

% Circular Shift
function y = cshift(signal, m)
% m: amount of shift
N = length(signal);
n = 0:N-1;
n = mod(n-m, N);
y = signal(floor(n+1));
end