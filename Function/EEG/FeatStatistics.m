function [ feat, feat_cnt, feat_ck ] = FeatStatistics( data, feat, feat_cnt, params )

%% STATISTICS FEATURE LIST
feat(feat_cnt).name     = 'stat_pow';
feat(feat_cnt+1).name   = 'stat_avg';
feat(feat_cnt+2).name   = 'stat_std';
feat(feat_cnt+3).name   = 'stat_diff1';
feat(feat_cnt+4).name   = 'stat_diff2';
feat(feat_cnt+5).name   = 'stat_diff1_n';
feat(feat_cnt+6).name   = 'stat_diff2_n';
for i = 1:7
    feat(feat_cnt + i - 1).idx  = [];
    feat(feat_cnt + i - 1).data = [];
    feat(feat_cnt + i - 1).label = [];
end
feat_ck = [];

%% EXTRACTION
for vid = 1:size(data.vid, 2)
    data_t = data.vid(vid).(params.feat_type)'; % ch x time
    if ~isempty(data_t)
        idx     = 1:size(data_t, 2);
        if length(idx) < ceil(params.overlap * params.sampling_rate)
            idx_m = idx';
        else
            idx_m   = buffer(idx, ceil(params.window * params.sampling_rate), ceil(params.overlap * params.sampling_rate), 'nodelay');
        end
        
        power_vid   = [];
        avg_vid     = [];
        std_vid     = [];
        diff1_vid   = [];
        diff2_vid   = [];
        diff1_n_vid = [];
        diff2_n_vid = [];
        
        if sum(idx_m(:, end) == 0) && size(idx_m, 2) ~= 1
            num_seg = size(idx_m, 2) - 1;
        else 
            num_seg = size(idx_m, 2);
        end
        for i = 1:num_seg
            idx_m_i     = idx_m(:, i);
            data_t_i    = data_t(:, idx_m_i(idx_m_i ~= 0));
            
            if params.hamm_on
                data_t_i = data_t_i .* repmat(hamming(ceil(size(data_t_i, 2)))', params.num_ch, 1);
            end
            
            power_i   = sum(data_t_i.^2, 2) / size(data_t_i, 2);
            avg_i     = mean(data_t_i, 2);
            std_i     = std(data_t_i, [], 2);
            diff1_i   = sum(diff(data_t_i, 1, 2), 2) / (size(data_t_i, 2) - 1);
            diff2_i   = sum(diff(data_t_i, 2, 2), 2) / (size(data_t_i, 2) - 2);
            diff1_n_i = diff1_i./std_i;
            diff2_n_i = diff2_i./std_i;
            
            power_vid   = [power_vid, power_i];
            avg_vid     = [avg_vid, avg_i];
            std_vid     = [std_vid, std_i];
            diff1_vid   = [diff1_vid, diff1_i];
            diff2_vid   = [diff2_vid, diff2_i];
            diff1_n_vid = [diff1_n_vid, diff1_n_i];
            diff2_n_vid = [diff2_n_vid, diff2_n_i];
            
            if sum(power_i > 500)
                feat_ck = [feat_ck; [1, max(max(data_t_i)), min(min(data_t_i))]];
            else
                feat_ck = [feat_ck; zeros(1, 3)];
            end
        end
        
        feat(feat_cnt).data = [feat(feat_cnt).data; power_vid'];
        feat(feat_cnt+1).data = [feat(feat_cnt+1).data; avg_vid'];
        feat(feat_cnt+2).data = [feat(feat_cnt+2).data; std_vid'];
        feat(feat_cnt+3).data = [feat(feat_cnt+3).data; diff1_vid'];
        feat(feat_cnt+4).data = [feat(feat_cnt+4).data; diff2_vid'];
        feat(feat_cnt+5).data = [feat(feat_cnt+5).data; diff1_n_vid'];
        feat(feat_cnt+6).data = [feat(feat_cnt+6).data; diff2_n_vid'];
        
        for i = 1:7
            feat(feat_cnt + i - 1).idx      = [feat(feat_cnt + i - 1).idx; repmat(vid, size(power_vid, 2), 1)];
            feat(feat_cnt + i - 1).label    = [feat(feat_cnt + i - 1).label; repmat(data.survey(vid, :), size(power_vid, 2), 1)];
        end
    end
end
feat_cnt = feat_cnt + 7;
fprintf('Done: Feature Extraction (Stat.) \n');


%% SINGLE VIDEO
% function [feat] = FeatStatistics( data, params )
% % data: time x ch
% data_t  = data'; % ch x time
% idx     = 1:size(data_t, 2);
% idx_m   = buffer(idx, ceil(params.window * params.sampling_rate), ceil(params.overlap * params.sampling_rate), 'nodelay');
%
% feat.power   = [];
% feat.avg     = [];
% feat.sd      = [];
% feat.diff1   = [];
% feat.diff2   = [];
% feat.diff1_n = [];
% feat.diff2_n = [];
% for i = 1:size(idx_m, 2)
%     idx_m_i     = idx_m(:, i);
%     data_t_i    = data_t(:, idx_m_i(idx_m_i ~= 0));
%
%     if params.hamm_on
%         data_t_i = data_t_i .* repmat(hamming(ceil(size(data_t_i, 2)))', params.num_ch, 1);
%     end
%
%     power   = sum(data_t_i.^2, 2) / size(data_t_i, 2);
%     avg     = mean(data_t_i, 2);
%     sd      = std(data_t_i, [], 2);
%     diff1   = sum(diff(data_t_i, 1, 2), 2) / (size(data_t_i, 2) - 1);
%     diff2   = sum(diff(data_t_i, 2, 2), 2) / (size(data_t_i, 2) - 2);
%     diff1_n = diff1./sd;
%     diff2_n = diff2./sd;
%
%     feat.power   = [feat.power, power];
%     feat.avg     = [feat.avg, avg];
%     feat.sd      = [feat.sd, sd];
%     feat.diff1   = [feat.diff1, diff1];
%     feat.diff2   = [feat.diff2, diff2];
%     feat.diff1_n = [feat.diff1_n, diff1_n];
%     feat.diff2_n = [feat.diff2_n, diff2_n];
% end




