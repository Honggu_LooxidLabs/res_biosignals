function [feat, feat_cnt] = FeatBandPower( data, feat, feat_cnt, params, type )
%% BANDPOWER FEATURE LIST
for i = 1:size(params.bands, 1)
    feat(feat_cnt + i - 1).name = ['bp_avg_b_', num2str(i)];
    feat(feat_cnt + i - 1).idx  = [];
    feat(feat_cnt + i - 1).data = [];
    feat(feat_cnt + i - 1).label = [];
    feat(feat_cnt + i - 1 + size(params.bands, 1)).name = ['bp_min_b_', num2str(i)];
    feat(feat_cnt + i - 1 + size(params.bands, 1)).idx  = [];
    feat(feat_cnt + i - 1 + size(params.bands, 1)).data = [];
    feat(feat_cnt + i - 1 + size(params.bands, 1)).label = [];
    feat(feat_cnt + i - 1 + size(params.bands, 1)*2).name = ['bp_max_b_', num2str(i)];
    feat(feat_cnt + i - 1 + size(params.bands, 1)*2).idx  = [];
    feat(feat_cnt + i - 1 + size(params.bands, 1)*2).data = [];
    feat(feat_cnt + i - 1 + size(params.bands, 1)*2).label = [];
    feat(feat_cnt + i - 1 + size(params.bands, 1)*3).name = ['bp_var_b_', num2str(i)];
    feat(feat_cnt + i - 1 + size(params.bands, 1)*3).idx  = [];
    feat(feat_cnt + i - 1 + size(params.bands, 1)*3).data = [];
    feat(feat_cnt + i - 1 + size(params.bands, 1)*3).label = [];
end
feat(end + 1).name  = 'bp_ab_ratio';
feat(end).idx   = [];
feat(end).data  = [];
feat(end).label = [];

%% EXTRACTION
for vid = 1:size(data.vid, 2)
    data_t = data.vid(vid).(params.feat_type)'; % ch x time
    
    if ~isempty(data_t)
        idx     = 1:size(data_t, 2);
        if length(idx) < ceil(params.overlap * params.sampling_rate)
            idx_m = idx';
        else
            idx_m   = buffer(idx, ceil(params.window * params.sampling_rate), ceil(params.overlap * params.sampling_rate), 'nodelay');
        end
        
        bp_avg_v = [];
        bp_min_v = [];
        bp_max_v = [];
        bp_var_v = [];
        if sum(idx_m(:, end) == 0) && size(idx_m, 2) ~= 1
            num_seg = size(idx_m, 2) - 1;
        else 
            num_seg = size(idx_m, 2);
        end
        for i = 1:num_seg
            idx_m_i = idx_m(:, i);
            data_i  = data_t(:, idx_m_i(idx_m_i ~= 0));
            
            switch type
                case 'matlab'
                    %             bp = [];
                    %             for b = 1:length(params.bands)
                    %                 bp = [bp, bandpower(data_i', 250, params.bands(b,:))'];
                    %             end
                case 'fft'
                    %             n = 2^nextpow2(size(data_i, 2));
                    n = size(data_i, 2);
                    data_fft        = fft(data_i, params.num_fft, 2);
                    data_fft_pow2   = abs(data_fft/n);
                    data_fft_pow1   = data_fft_pow2(:, 1:floor(n/2)+1);
                    data_fft_pow1(:, 2:end-1) = 2*data_fft_pow1(:, 2:end-1);
                    data_fft_freq   = params.sampling_rate*(0:(n/2))/n;
                    data_fft_pow    = data_fft_pow1;
                    
                    bp_avg_i = [];
                    bp_min_i = [];
                    bp_max_i = [];
                    bp_var_i = [];
                    for b = 1:length(params.bands)
                        b_idx = data_fft_freq >= params.bands(b,1) & data_fft_freq <= params.bands(b,2);
                        bp_avg_i = [bp_avg_i, mean(data_fft_pow(:, b_idx), 2)];
                        bp_min_i = [bp_min_i min(data_fft_pow(:, b_idx), [], 2)];
                        bp_max_i = [bp_max_i max(data_fft_pow(:, b_idx), [], 2)];
                        bp_var_i = [bp_var_i var(data_fft_pow(:, b_idx), [], 2)];
                    end
                case 'stft'
            end
            bp_avg_v = [bp_avg_v, bp_avg_i];
            bp_min_v = [bp_min_v, bp_min_i];
            bp_max_v = [bp_max_v, bp_max_i];
            bp_var_v = [bp_var_v, bp_var_i];
        end
        
        for i = 1:size(params.bands, 1)
            feat(feat_cnt + i - 1).idx  = [feat(feat_cnt + i - 1).idx; repmat(vid, size(bp_avg_v, 2)/size(params.bands, 1), 1)];
            feat(feat_cnt + i - 1).data = [feat(feat_cnt + i - 1).data; bp_avg_v(:, i:size(params.bands, 1):end)'];
            feat(feat_cnt + i - 1).label = [feat(feat_cnt + i - 1).label; repmat(data.survey(vid, :), size(bp_avg_v, 2)/size(params.bands, 1), 1)];
            feat(feat_cnt + i - 1 + size(params.bands, 1)).idx  = [feat(feat_cnt + i - 1 + size(params.bands, 1)).idx; repmat(vid, size(bp_min_v, 2)/size(params.bands, 1), 1)];
            feat(feat_cnt + i - 1 + size(params.bands, 1)).data = [feat(feat_cnt + i - 1 + size(params.bands, 1)).data; bp_min_v(:, i:size(params.bands, 1):end)'];
            feat(feat_cnt + i - 1 + size(params.bands, 1)).label = [feat(feat_cnt + i - 1 + size(params.bands, 1)).label; repmat(data.survey(vid, :), size(bp_min_v, 2)/size(params.bands, 1), 1)];
            feat(feat_cnt + i - 1 + size(params.bands, 1)*2).idx  = [feat(feat_cnt + i - 1 + size(params.bands, 1)*2).idx; repmat(vid, size(bp_max_v, 2)/size(params.bands, 1), 1)];
            feat(feat_cnt + i - 1 + size(params.bands, 1)*2).data = [feat(feat_cnt + i - 1 + size(params.bands, 1)*2).data; bp_max_v(:, i:size(params.bands, 1):end)'];
            feat(feat_cnt + i - 1 + size(params.bands, 1)*2).label = [feat(feat_cnt + i - 1 + size(params.bands, 1)*2).label; repmat(data.survey(vid, :), size(bp_max_v, 2)/size(params.bands, 1), 1)];
            feat(feat_cnt + i - 1 + size(params.bands, 1)*3).idx  = [feat(feat_cnt + i - 1 + size(params.bands, 1)*3).idx; repmat(vid, size(bp_var_v, 2)/size(params.bands, 1), 1)];
            feat(feat_cnt + i - 1 + size(params.bands, 1)*3).data = [feat(feat_cnt + i - 1 + size(params.bands, 1)*3).data; bp_var_v(:, i:size(params.bands, 1):end)'];
            feat(feat_cnt + i - 1 + size(params.bands, 1)*3).label = [feat(feat_cnt + i - 1 + size(params.bands, 1)*3).label; repmat(data.survey(vid, :), size(bp_var_v, 2)/size(params.bands, 1), 1)];
        end
    end
end
    
feat(end).data  = feat(end-17).data ./ feat(end-18).data; % beta/alpha ratio
feat(end).idx   = feat(end-17).idx;
feat(end).label = feat(end-17).label;
feat_cnt = feat_cnt + (4*5 + 1);
fprintf('Done: Feature Extraction (BandPower)\n');


