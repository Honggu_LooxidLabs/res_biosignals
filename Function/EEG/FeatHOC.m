function [ feat, feat_cnt ] = FeatHOC( data, feat, feat_cnt, params )
%% HOC FEATURE LIST
for i = 1:params.hoc_j
    feat(feat_cnt + i - 1).name = ['hoc_k_', num2str(i)];
    feat(feat_cnt + i - 1).idx  = [];
    feat(feat_cnt + i - 1).data = [];
    feat(feat_cnt + i - 1).label = [];
end

%% EXTRACTION
for vid = 1:size(data.vid, 2)
    data_t = data.vid(vid).(params.feat_type)'; % ch x time
    
    if ~isempty(data_t)
        idx     = 1:size(data_t, 2);
        if length(idx) < ceil(params.overlap * params.sampling_rate)
            idx_m = idx';
        else
            idx_m   = buffer(idx, ceil(params.window * params.sampling_rate), ceil(params.overlap * params.sampling_rate), 'nodelay');
        end
        
        hoc_v = [];
        if sum(idx_m(:, end) == 0) && size(idx_m, 2) ~= 1
            num_seg = size(idx_m, 2) - 1;
        else 
            num_seg = size(idx_m, 2);
        end
        for i = 1:num_seg
            idx_m_i = idx_m(:, i);
            data_i = data_t(:, idx_m_i(idx_m_i ~= 0));
            
            %     if params.hamm_on
            %         data_i = data_i .* repmat(hamming(ceil(size(data_i, 2)))', params.num_ch, 1);
            %     end
            
            backward_diff       = zeros(params.hoc_j, size(data_i, 2), params.num_ch);
            backward_diff_accum = zeros(params.hoc_j, size(data_i, 2), params.num_ch);
            zero_crossing_count = zeros(params.hoc_j, size(data_i, 2), params.num_ch);
            for j = 1:params.hoc_j
                backward_diff(j, 1:end-params.hoc_j+j, :) = nchoosek(params.hoc_j-1, j-1) * (-1)^(j-1) * data_i(:, params.hoc_j-j+1:end)';
                backward_diff_accum(j,:, :) = sum(backward_diff);
                zero_crossing_count(j,:, :) = backward_diff_accum(j,:, :) >= 0;
            end
            hoc_k = reshape(sum((zero_crossing_count(:, 2:end,:) - zero_crossing_count(:, 1:end-1, :)).^2, 2), params.hoc_j, params.num_ch);
            hoc_v = [hoc_v, hoc_k'];
        end
        
        for i = 1:params.hoc_j
            feat(feat_cnt + i - 1).idx  = [feat(feat_cnt + i - 1).idx; repmat(vid, size(hoc_v, 2)/params.hoc_j, 1)];
            feat(feat_cnt + i - 1).data = [feat(feat_cnt + i - 1).data; hoc_v(:, i:params.hoc_j:end)'];
            feat(feat_cnt + i - 1).label = [feat(feat_cnt + i - 1).label; repmat(data.survey(vid, :), size(hoc_v, 2)/params.hoc_j, 1)];
        end

    end
end
feat_cnt = feat_cnt + params.hoc_j;
fprintf('Done: Feature Extraction (HOC)\n');

%% SINGLE VIDEO
% function [ feat ] = FeatHOC( data, params )
% % params.hoc_j = params.hoc_j+1;
%
% data    = data'; % ch x time
% idx     = 1:size(data, 2);
% idx_m   = buffer(idx, ceil(params.window * params.sampling_rate), ceil(params.overlap * params.sampling_rate), 'nodelay');
%
% hoc_all = [];
% for i = 1:size(idx_m, 2)
%     idx_m_i = idx_m(:, i);
%     data_i = data(:, idx_m_i(idx_m_i ~= 0));
%
%     %     if params.hamm_on
%     %         data_i = data_i .* repmat(hamming(ceil(size(data_i, 2)))', params.num_ch, 1);
%     %     end
%
%     backward_diff       = zeros(params.hoc_j, size(data_i, 2), params.num_ch);
%     backward_diff_accum = zeros(params.hoc_j, size(data_i, 2), params.num_ch);
%     zero_crossing_count = zeros(params.hoc_j, size(data_i, 2), params.num_ch);
%     for j = 1:params.hoc_j
%         backward_diff(j, 1:end-params.hoc_j+j, :) = nchoosek(params.hoc_j-1, j-1) * (-1)^(j-1) * data_i(:, params.hoc_j-j+1:end)';
%         backward_diff_accum(j,:, :) = sum(backward_diff);
%         zero_crossing_count(j,:, :) = backward_diff_accum(j,:, :) >= 0;
%     end
%     hoc_k = reshape(sum((zero_crossing_count(:, 2:end,:) - zero_crossing_count(:, 1:end-1, :)).^2, 2), params.hoc_j, params.num_ch);
%     hoc_all = [hoc_all, hoc_k'];
% end
% hoc = [];
% for j = 1:params.hoc_j
%     f_name = ['k_', num2str(j)];
%     hoc.(f_name) = hoc_all(:, j:params.hoc_j:end);
% end
%
% feat = hoc;