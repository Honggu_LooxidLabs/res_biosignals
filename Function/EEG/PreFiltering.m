function [ data_filt ] = PreFiltering( data, params, type )

%% COMMON AVERAGE REFERENCE
data_car = PreCAR (data);

switch type
    case 'iir'
        %% FILTERING (IIR)
        data_car_high   = PreFilter( data_car, params, 'iir', 'high');
        data_car_stop   = PreFilter( data_car_high, params, 'iir', 'stop');
        data_car_bandpass = PreFilter( data_car_stop, params, 'iir', 'bandpass');
        
    case 'fir'
        %% FILTERING (FIR)
        data_car_high   = PreFilter( data_car, params, 'fir', 'high');
        data_car_stop   = PreFilter( data_car_high, params, 'fir', 'stop');
        data_car_bandpass = PreFilter( data_car_stop, params, 'fir', 'bandpass');
end

%% FINAL OUTPUT
data_filt = data_car_bandpass;

end