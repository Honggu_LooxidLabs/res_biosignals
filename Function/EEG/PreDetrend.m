function data_detrend = PreDetrend ( data )

for i = 1:min(size(data))
    [p, ~, mu]  = polyfit((1:numel(data(:, i)))', data(:, i), 6); % order 6
    f_y         = polyval(p, (1:numel(data(:, i)))', [], mu);
    data_detrend(:, i) = data(:, i) - f_y;
end
