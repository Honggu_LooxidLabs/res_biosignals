function [ feat, feat_cnt ] = FeatFD( data, feat, feat_cnt, params )
%% FD FEATURE LIST
feat(feat_cnt).name = 'fd';
feat(feat_cnt).idx  = [];
feat(feat_cnt).data = [];
feat(feat_cnt).label = [];

%% EXTRACTION
% m: 1 to k, initial time
% k: time interval

for vid = 1:size(data.vid, 2)
    data_t = data.vid(vid).(params.feat_type)'; % ch x tim
    
    if ~isempty(data_t)
        idx     = 1:size(data_t, 2);
        if length(idx) < ceil(params.overlap * params.sampling_rate)
            idx_m = idx';
        else
            idx_m   = buffer(idx, ceil(params.window * params.sampling_rate), ceil(params.overlap * params.sampling_rate), 'nodelay');
        end
        fd_v = [];
        if sum(idx_m(:, end) == 0) && size(idx_m, 2) ~= 1
            num_seg = size(idx_m, 2) - 1;
        else 
            num_seg = size(idx_m, 2);
        end
        for i = 1:num_seg
            idx_m_i = idx_m(:, i);
            data_i = data_t(:, idx_m_i(idx_m_i ~= 0));
            
            if params.hamm_on
                data_i = data_i .* repmat(hamming(ceil(size(data_i, 2)))', params.num_ch, 1);
            end
            
            T = size(data_i, 2);
            for m = 1:params.fd_k
                Lmki = 0;
                for j = 1:fix((T-m)/params.fd_k)
                    Lmki = Lmki + abs(data_i(:, m+j*params.fd_k) - data_i(:, m+(j-1)*params.fd_k));
                end
                temp = (T-1) / (fix((T-m)/params.fd_k) * params.fd_k);
                Lmk(:, m) = (Lmki*temp) / params.fd_k^2;
            end
            Lk = mean(Lmk, 2);
            fd_i = -1 * log(Lk) / log(params.fd_k);
            fd_v = [fd_v, fd_i];
        end
        
        feat(feat_cnt).data = [feat(feat_cnt).data; fd_v'];
        feat(feat_cnt).idx  = [feat(feat_cnt).idx; repmat(vid, size(fd_v, 2), 1)];
        feat(feat_cnt).label = [feat(feat_cnt).label; repmat(data.survey(vid, :), size(fd_v, 2), 1)];
    end
end
feat_cnt = feat_cnt + 1;
fprintf('Done: Feature Extraction (FD)\n');