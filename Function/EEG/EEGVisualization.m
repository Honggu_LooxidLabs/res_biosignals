function EEGVisualization(varargin)
%% COLOR
col =  [0       0       0
    0         0.4470    0.7410
    0.8500    0.3250    0.0980
    0.9290    0.6940    0.1250
    0.4940    0.1840    0.5560
    0.4660    0.6740    0.1880
    0.3010    0.7450    0.9330
    0.6350    0.0780    0.1840];

%% PLOT SETTING
num_subplot = length(varargin);
figure;
set(gcf,'color','w');
for n = 1:num_subplot
    for i = 1:6
        ax = axes('XAxisLocation', 'origin', 'Box', 'off');
        subplot(6, num_subplot, n+((i-1)*num_subplot), ax);
        plot(varargin{n}(:, i), 'Color', col(i,:));
        xlim([0 size(varargin{n}, 1)]);
%         if std(varargin{n}(:, i)) < 100
%         ylim([-100 100]);
%         end
        title(sprintf('Ch.%d', i));
        
    end
end
