function [ data_car, ref ] = PreCAR( data )

ref = mean(data);
data_car = data - repmat(ref, size(data, 1), 1);

end

