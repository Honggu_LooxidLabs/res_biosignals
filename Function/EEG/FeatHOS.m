function [ feat ] = FeatHOS( data, params )
params.hoc_j = params.hoc_j+1;

data    = data'; % ch x time
idx     = 1:size(data, 2);
idx_m   = buffer(idx, ceil(params.window * params.sampling_rate), ceil(params.overlap * params.sampling_rate), 'nodelay');

feat    = [];
bis     = [];
bis2    = [];
bic     = [];
bic2    = [];
for i = 1:size(idx_m, 2)
    idx_m_i = idx_m(:, i);
    data_i = data(:, idx_m_i(idx_m_i ~= 0));
    
    if params.hamm_on
        data_i = data_i .* repmat(hamming(ceil(size(data_i, 2)))', params.num_ch, 1);
    end

    data_fft        = fft(data_i, params.num_fft, 2);
    data_fft_conj   = conj(data_fft);
    data_ps  = data_fft .* conj(data_fft); % power sepctrum
    data_frq = params.sampling_rate * (0:(params.num_fft)-1)/params.num_fft;
    
    
    [Bspec,waxis] = bispecd (data_i(1, :), params.num_fft, 0, params.num_fft, 0);
    

    bands_bis   = [];
    bands_bis2  = [];
    bands_bic   = [];
    bands_bic2  = [];
    for b1 = 1:length(params.bands)
        for b2 = 1:length(params.bands)
            
            sum_Bic = 0;
            sum_Bis = 0;
            squared_sum_Bic = 0;
            squared_sum_Bis = 0;
            
            [~, f1_min_idx] = min(abs(data_frq - params.bands(b1,1))); 
            [~, f1_max_idx] = min(abs(data_frq - params.bands(b1,2))); 
            [~, f2_min_idx] = min(abs(data_frq - params.bands(b2,1))); 
            [~, f2_max_idx] = min(abs(data_frq - params.bands(b2,2))); 
            
            for f1_idx = f1_min_idx:f1_max_idx
                for f2_idx = f2_min_idx:f2_max_idx
                    f1_f2 = data_frq(f1_idx) + data_frq(f2_idx);    % frequency f1 + f2
                    [~, f1_f2_idx] = min(abs(data_frq - f1_f2));    % get index of closest frequency index
                    cur_Bis = data_fft(:, f1_idx) .* data_fft(:, f2_idx) .* data_fft_conj(:, f1_f2_idx);
                    sum_Bis = sum_Bis + abs(cur_Bis);               % sum of bispectra magnitudes
                    squared_sum_Bis = squared_sum_Bis + abs(cur_Bis) .* abs(cur_Bis);
                    
                    P_f1    = real(data_ps(f1_idx));
                    P_f2    = real(data_ps(f2_idx));
                    P_f1_f2 = real(data_ps(f1_f2_idx));
                    cur_Bic = abs(cur_Bis) / sqrt(P_f1 * P_f2 * P_f1_f2);   % sum of bicoherence magnitudes
                    sum_Bic = sum_Bic + cur_Bic;  
                    squared_sum_Bic = squared_sum_Bic + cur_Bic.^2;         % sum of squared bicoherence magnitudes
                end
            end
            bands_bis   = [bands_bis, sum_Bis ];
            bands_bis2  = [bands_bis2, squared_sum_Bis];
            bands_bic   = [bands_bic, sum_Bic];
            bands_bic2  = [bands_bic2, squared_sum_Bic];
        end
    end
    
    
    
    
    
    bis     = [bis, bands_bis];
    bis2    = [bis2, bands_bis2];
    bic     = [bic, bands_bic];
    bic2    = [bic2, bands_bic2];
end

feat = [];
for j = 1:length(params.bands)
    f_name = ['b_', num2str(j)];
    feat.bis.(f_name)   = bis(:, j:length(params.bands):end);
    feat.bis2.(f_name)  = bis2(:, j:length(params.bands):end);
    feat.bic.(f_name)   = bic(:, j:length(params.bands):end);
    feat.bic2.(f_name)  = bic2(:, j:length(params.bands):end);
end

