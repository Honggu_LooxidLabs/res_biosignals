function [ feat, feat_cnt ] = FeatHHS( data, feat, feat_cnt, params )
%% HHS FEATURE LIST
for i = 1:size(params.bands, 1)
    feat(feat_cnt + i - 1).name = ['hhs_b_', num2str(i)];
    feat(feat_cnt + i - 1).idx  = [];
    feat(feat_cnt + i - 1).data = [];
    feat(feat_cnt + i - 1).label = [];
end

%% EXTRACTION
for vid = 1:size(data.vid, 2)
    vid
    data_t = data.vid(vid).(params.feat_type)'; % ch x time
    
    if ~isempty(data_t)
        idx         = 1:size(data_t, 2);
        if length(idx) < ceil(params.overlap * params.sampling_rate)
            idx_m = idx';
        else
            idx_m   = buffer(idx, ceil(params.window * params.sampling_rate), ceil(params.overlap * params.sampling_rate), 'nodelay');
        end
        hhs         = [];
        if sum(idx_m(:, end) == 0) && size(idx_m, 2) ~= 1
            num_seg = size(idx_m, 2) - 1;
        else 
            num_seg = size(idx_m, 2);
        end
        for i = 1:num_seg
            idx_m_i = idx_m(:, i);
            data_i  = data_t(:, idx_m_i(idx_m_i ~= 0));
            win_sz  = floor(size(data_i, 2) / params.hhs_w);
            
            hhs_ch  = [];
            for ch = 1:size(data_i, 1)
                data_i_ch = data_i(ch, :);
                imf     = EMD(data_i_ch);
                a = [];
                f = [];
                for k = 1:length(imf)
                    a(k) = sum(imf{k} .* imf{k});
                    th   = angle(hilbert(imf{k}));
                    f_k  = diff(th)/(1/params.sampling_rate)/(2*pi);
                    f_k(f_k < 1) = 0;
                    f(k, :) = f_k;
                end
                a = 1-a/max(a);
                feat_matirx = [];
                for win = 1:params.hhs_w
                    if win == params.hhs_w
                        f_k_w = f(:, (win-1)*win_sz+1:end);
                    else
                        f_k_w = f(:, (win-1)*win_sz+1:win_sz*win);
                    end
                    tf_fb_all = [];
                    for b = 1:size(params.bands,1)
                        n_fb    = sum(sum(f_k_w >= params.bands(b, 1) & f_k_w < params.bands(b, 2), 2));
                        tf_fb   = sum(sum(f_k_w >= params.bands(b, 1) & f_k_w < params.bands(b, 2), 2) .* a' ./n_fb);
                        tf_fb_all = [tf_fb_all; tf_fb];
                    end
                    feat_matirx = [feat_matirx, tf_fb_all'];
                end
                hhs_ch = [hhs_ch; feat_matirx];
            end
            hhs = [hhs, hhs_ch];
        end
        
        for i = 1:size(params.bands, 1)
            feat(feat_cnt + i - 1).idx  = [feat(feat_cnt + i - 1).idx; repmat(vid, size(hhs, 2)/size(params.bands, 1), 1)];
            feat(feat_cnt + i - 1).data = [feat(feat_cnt + i - 1).data; hhs(:, i:size(params.bands, 1):end)'];
            feat(feat_cnt + i - 1).label = [feat(feat_cnt + i - 1).label; repmat(data.survey(vid, :), size(hhs, 2)/size(params.bands, 1), 1)];
        end
    end
end
feat_cnt = feat_cnt + size(params.bands, 1);
fprintf('Done: Feature Extraction (HHS)\n');
end


%% get intrinsic mode functions(IMFs) using empirical mode decomposition (EMD)
function imf = EMD( signal )
imf = [];
cnt = 1;
while ~ismonotonic(signal)
    h1 = signal;
    sd = inf; % standard deviation
    while (sd > 0.1) || ~isimf(h1)
        cnt = cnt+1;
        upper_envelope = getspline(h1);
        lower_envelope = -getspline(-h1);
        h2 = h1 - (upper_envelope + lower_envelope)/2;
        
        sd = sum((h2 - h1).^2) / sum(h1.^2);
        h1 = h2;
        if cnt > 1000
            break
        end
    end
    imf{end+1}  = h1;
    signal      = signal - h1;
end
imf{end+1}  = signal;
end

%% monotonic test
function m = ismonotonic( signal )
p_pos = peak(signal);
p_neg = peak(-signal);
l = length(p_pos) * length(p_neg);

if l > 0
    m = 0;
else
    m = 1;
end
end

%% imf test
% IMF is defined as a function that satisfies the following requirements:
% 1. In the whole data set, the number of extrema and the number of zero-crossings must either be equal or differ at most by one.
% 2. At any point, the mean value of the envelope defined by the local maxima and the envelope defined by the local minima is zero.
function u = isimf( signal )
n = length(signal);
u1 = sum(signal(1:n-1) .* signal(2:n) < 0 );

p_pos = peak(signal);
p_neg = peak(-signal);
u2 = length(p_pos) + length(p_neg);

if abs(u1 - u2) > 1
    u = 0;
else
    u = 1;
end
end

%% find the number of peak in signal
function p = peak( signal )
p       = find(diff(diff(signal) > 0) < 0);
p_idx   = find(signal(p+1) > signal(p));
p(p_idx) = p(p_idx) + 1;
end

%% spline
function s = getspline( signal )
n = length(signal);
p = peak(signal);
s = spline([0 p n+1],[0 signal(p) 0], 1:n);
end
