function data_filt = PreFilter( data, params, type1, type2 )
% Nyquist frequency
f_n = params.sampling_rate/2;

switch type1
    case 'iir'
        filt_order = params.butter_order;
        switch type2
            case 'high'
                [B, A] = butter(filt_order, params.highpass_fq/f_n, type2);
            case 'low'
                [B, A] = butter(filt_order, params.lowpass_fq/f_n, type2);
            case 'stop'
                [B, A] = butter(filt_order, [params.stop_fq-1 params.stop_fq+1] ./f_n, type2);
            case 'bandpass'
                [B, A] = butter(filt_order, params.bandpass_fq/f_n, type2);
        end
        data_filt = filtfilt(B, A, data); % ft function
        
    case 'fir'
        switch type2
            case 'high'
                filt_order = 3*fix(params.sampling_rate / params.highpass_fq );
                if rem(filt_order, 2) == 1
                    filt_order = filt_order + 1;
                end
                if filt_order > floor((size(data, 1) - 1) / 3)
                    filt_order = floor(size(data, 1)/3) - 2;
                    if rem(filt_order, 2) == 1
                        filt_order = filt_order + 1;
                    end
                end
                B = fir1(filt_order, max(params.highpass_fq)/params.sampling_rate, 'high');
                A = 1;
                
            case 'low'
                filt_order = 3*fix(params.sampling_rate / params.lowpass_fq );
                if filt_order > floor((size(data, 1) - 1) / 3)
                    filt_order = floor(size(data, 1)/3) - 1;
                end
                B = fir1(filt_order, max(params.lowpass_fq)/params.sampling_rate);
                A = 1;
                
            case 'stop'
                filt_order = 3*fix(params.sampling_rate / (params.stop_fq + 1) );
                if rem(filt_order, 2) == 1
                    filt_order = filt_order + 1;
                end
                if filt_order > floor((size(data, 1) - 1) / 3)
                    filt_order = floor(size(data, 1)/3) - 2;
                    if rem(filt_order, 2) == 1
                        filt_order = filt_order + 1;
                    end
                end
                B = fir1(filt_order, [params.stop_fq-1 params.stop_fq+1]/params.sampling_rate, 'stop');
                A = 1;               
                
            case 'bandpass'
                filt_order = 3*fix(params.sampling_rate / max(params.bandpass_fq) );
                if filt_order > floor((size(data, 1) - 1) / 3)
                    filt_order = floor(size(data, 1)/3) - 1;                    
                end
                B = fir1(filt_order, [params.bandpass_fq(1) params.bandpass_fq(2)]/params.sampling_rate);
                A = 1;
        end
        data_filt = filtfilt(B, A, data);
end
end

