function [data_denoised, data_noise] = ArtifactRemoval(data, params, type, vis)

switch type
    case 'jump'
        if nargin <= 3
            vis = 0;
        end
        data_denoised   = [];
        data_noise      = [];
        for ch = 1:6
            data_ch                 = data(:, ch);
            [data_ch_clean, noise, ~]   = wdscore(data_ch', 'wavelet', 'la8', 'threshold', 20);
%             [data_ch_clean, noise, ~]   = wdscore(data_ch', 'wavelet', 'la16', 'verbose', 0);
            data_noise      = [data_noise, noise];
            data_denoised   = [data_denoised, data_ch_clean];
        end
        if vis == 1
            EEGVisualization(data, data_noise, data_denoised);
            suptitle('Despike')
        end
        
    case 'blink'
        if nargin <= 3
            vis = 0;
        end
%         [ica_w, ica_s]  = runica(data', 'lrate', 0.0001);
        [ica_w, ica_s]  = runica(data', 'verbose', 'off');
        unmixing        = ica_w * ica_s;
        mixing          = inv(unmixing);
        components      = (unmixing * data')';
        fft_length      = 2^nextpow2(size(components, 1));
        fft_freq_range  = params.sampling_rate*(0:(fft_length/2))/fft_length;
        components_pow  = pwelch(components(fft_freq_range < 10, :)); % 10 Hz
        
        components_pow_r = zeros(1, 6);
        for ch = 1:6
            [comp_peak, ~] = findpeaks(components_pow(:, ch));
            if length(comp_peak) > 4
                components_pow_r(ch) = max(comp_peak(1:3)) / max(comp_peak(4:end));
            else
                components_pow_r(ch) = max(comp_peak(1)) / max(comp_peak(end));
            end
        end
        [~, comp_rejection_idx] = max(components_pow_r);
        tra             = eye(size(data', 1)) - mixing(:, comp_rejection_idx)*unmixing(comp_rejection_idx, :);
        data_denoised   = (full(tra) * data')';           
        
        if vis == 1
            comp_rejection_idx
            EEGVisualization(data, components, data_denoised);
            suptitle(sprintf('Rejected: %.2d Comp.', comp_rejection_idx))
        end
        data_noise = components;
end


