clear 
close all

%% LOAD DATA
load('samples_ecg_openbci.mat'); 
ecg_openbci(isnan(ecg_openbci)) = [];
ecg_raw = ecg_openbci - mean(ecg_openbci);

%% PARAMETERS
fs          = 250;
K           = 2; %kmeans
sq_param    = 1;
filt_order  = 3;
cutoff_band = [0.5 30];
win_size    = (fs * 512)/1000;
if rem(win_size, 2) ==0; win_size = win_size + 1; end

%% REMOVE SEGMENT
ecg_raw = ecg_raw(fs*5+1:end);

%% FILTERING
[b, a] = butter(filt_order, [59 61]/(fs/2), 'stop');
ecg_filt        = filtfilt(b, a, ecg_raw);
[b, a] = butter(filt_order, cutoff_band/(fs/2), 'bandpass');
ecg_filt        = filtfilt(b, a, ecg_filt);
% ecg_filt_smooth = sgolayfilt(ecg_filt, 5, 51);
ecg_filt_smooth = smoothdata(ecg_filt, 'movmedian', 10);

%% DERIVATIVE
b = [1 2 0 -2 -1].*(1/8)*fs;   
ecg_filt_smooth_derv = filtfilt(b, 1, ecg_filt_smooth);
ccg_filt_smooth_derv = ecg_filt_smooth_derv/max(ecg_filt_smooth_derv);

%% FIND PEAKS
% R
[peaks_all, locs_all]   = findpeaks(ecg_filt.^sq_param, 'MINPEAKDISTANCE', round(0.2*fs)); %PARAMS
peaks_all               = peaks_all.^(1/sq_param);

peaks_idx = zeros(length(peaks_all), 1);
while sum(peaks_idx == 1) < length(peaks_idx) * 0.30 || sum(peaks_idx == 2) < length(peaks_idx) * 0.30 
    peaks_idx = kmeans(peaks_all, K);    
end

peaks_R_idx = 1;
for k = 1:K
    peak_sub_mean = mean(peaks_all(peaks_idx == k));
    if k ~= 1
        if mean(peaks_all(peaks_idx == k)) - mean(peaks_all(peaks_idx == peaks_R_idx)) > 0
            peaks_R_idx = k;
        end
    end
end

peaks_R = peaks_all(peaks_idx == peaks_R_idx);
locs_R  = locs_all(peaks_idx == peaks_R_idx);

plot(ecg_filt); hold on
plot(locs_R, peaks_R, 'o');
plot(locs_all, peaks_all, '*');

%% FEATURES
RR_interval_all = (locs_R(2:end) - locs_R(1:end-1)) ./ fs;
mean_RR = mean(RR_interval_all);
std_RR  = std(RR_interval_all);

% HR_10sec_all    = [];
% seg_idx         = buffer(1:length(ecg_filt), fs * 10);
% for i = 1:size(seg_idx, 2)
%     tmp_idx = seg_idx(:, i);
%     tmp_idx(tmp_idx == 0) = [];
%     
%     seg_ecg     = ecg_filt(tmp_idx);
%     seg_num_R   = sum(locs_R <= max(tmp_idx) & locs_R >= min(tmp_idx));
%     seg_HR      = (seg_num_R / (length(tmp_idx) / fs ))*60;
%     
%     HR_10sec_all = [HR_10sec_all; seg_HR];
% end
% mean_HR = mean(HR_10sec_all);
% std_HR  = std(HR_10sec_all);

HR = 60 ./ RR_interval_all;
mean_HR = mean(HR);
std_HR  = std(HR);
RR_diff     = RR_interval_all(2:end) - RR_interval_all(1:end-1);
RR_diff_n   = sum(RR_diff) / length(RR_diff);
RMSSD   = sqrt(sum(RR_diff.^2) / length(RR_diff));
SDSD    = sum(RR_diff - RR_diff_n) / length(RR_diff);
PNN50   = sum(RR_diff > 0.05) / length(RR_diff);



